﻿using UnityEngine;

public class MusicPlayer : MonoBehaviour {
	static MusicPlayer instance = null;

	void Awake () {
		Debug.Log ("Musicplayer Awake" + GetInstanceID ());
		// on utilise un signleton pour éviter que le musicPlayer se relance à chaque chargement de la scene qui le contient (start Menu)
		if (instance == null) {
			instance = this;
			// permet de ne pas supprimer la musique de fondlorque l'on quitte la scene
			GameObject.DontDestroyOnLoad (this);
			audio.Play ();
			Debug.Log ("Musicplayer Play " + GetInstanceID ());
			
		} else {
			Destroy (gameObject);
			print ("Duplicate MusicPlayer Self destruction");
		}
	}

	void Start () {
		Debug.Log ("Musicplayer Start" + GetInstanceID ());
		
	}

}
