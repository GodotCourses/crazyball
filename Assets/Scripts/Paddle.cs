using UnityEngine;

public class Paddle : MonoBehaviour {
	[Tooltip("position minimale en X pour le paddle")]
	public float
		xPosMin = 0.6f;
	[Tooltip("position maximale en X pour le paddle")]
	public float
		xPosMax = 15.4f;
	[Tooltip("Activer pour que le paddle suive automatiquement la balle")]
	public bool
		autoPlay;
	Vector3 paddlePos = new Vector3 (8.0f, 0.5f, 0.0f);
	Ball ball;

	void Start () {
		// permet d'éviter que le paddle soit mal positionné au départ du jeu 
		this.transform.position = paddlePos;
		ball = GameObject.FindObjectOfType<Ball> ();
	}

	void FixedUpdate () {
		if (!autoPlay) {
			MoveWithMouse ();
		} else {
			AutoPlay ();
		}
	}
	
	void AutoPlay () {
		paddlePos = new Vector3 (0.0f, transform.position.y, 0.0f);	
		Vector3 ballPos = ball.transform.position;
		paddlePos.x = Mathf.Clamp (ballPos.x, xPosMin, xPosMax);
		transform.position = paddlePos;
	}
	
	void MoveWithMouse () {
		float mouseXposInBlocs;
		float mouseYposInBlocs;
		mouseXposInBlocs = Mathf.Clamp ((Input.mousePosition.x / Screen.width * 16.0f), xPosMin, xPosMax);
		
		//ajout de la prise en compte du déplacement de la souris en Y pour pour le paddle
		mouseYposInBlocs = Mathf.Clamp ((Input.mousePosition.y / Screen.height * 12.0f), 0.5f, 4.5f);
		
		paddlePos = new Vector3 (mouseXposInBlocs, mouseYposInBlocs, 0.0f);
		transform.position = paddlePos;	
	}
}
 